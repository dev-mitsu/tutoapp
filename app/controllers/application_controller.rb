class ApplicationController < ActionController::Base

  def logged_in?
    redirect_to login_url unless current_user
  end

end

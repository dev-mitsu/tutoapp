class TweetsController < ApplicationController
  include SessionsHelper
  before_action :correct_user, only: :destroy


  def new
    @tweet = Tweet.new
  end

  def create
    @tweet = current_user.tweets.build(tweet_params)
    if @tweet.save
      redirect_to root_url
    else
      render 'new'
    end
  end

  def index
    @tweets = current_user.following_tweets
                  .page(params[:page]).per(5)
  end

  # views/tweets/index.html.erb

  def destroy
    @tweet.destroy
    redirect_to root_url
  end

  private

    def tweet_params
      params.require(:tweet).permit(:content, :picture)
    end

    def correct_user # そのツイートは本当にあなたのもの？？
      @tweet = current_user.tweets.find_by(id: params[:id])
      redirect_to root_url if @tweet.nil?
    end

    def following_tweets(user)
      tweets = []
      user.all_following.each do |user|
        tweets.concat(user.tweets.last(10))
      end
      Tweet.where(id: tweets.map{|tweet| tweet.id})
          .order(created_at: :desc).page(params[:page]).per(5)
    end
end


# $ rails g controller tweets new




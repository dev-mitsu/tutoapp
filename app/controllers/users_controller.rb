class UsersController < ApplicationController
  include SessionsHelper
  before_action :correct_user?, only: [ :edit, :update, :destroy ]

  def new
    @user = User.new #form_forメソッドに渡すため、空のユーザーオブジェクトの作成
  end

  def create
    @user = User.new(user_params)
    #①フォームに入力された値を取得し、
    #　それを元にユーザーオブジェクトを作成
    #②①で作られたユーザーオブジェクトをDBに保存する
    if @user.save
      # 成功した場合
      redirect_to root_url #トップページに戻る(redirect_toの際は_pathではなく_urlを用いる)
    else
      # 失敗した場合
      render 'new' #新規登録ページを描写(レンダリング)
    end
  end

  def edit
    @user = User.find(params[:id]) #URLのクエリパラメータからユーザーオブジェクトを取得
  end

  def update
    @user = User.find(params[:id])
    if @user.update(user_params) #updateメソッドを用いて情報を更新
      #成功時
      redirect_to root_url
    else
      #失敗時
      render 'edit' #編集ページを描写(レンダリング)
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy #destroyメソッドを用いて削除
    redirect_to root_url
  end

  def show
    @user = User.find(params[:id])
    @tweets = @user.tweets.page(params[:page]).per(5) #ユーザーに紐づくツイートを取得。ツイート一覧パーシャルに引き渡す。
  end

  # app/views/users/show.html.erb


  ### ここからフォロー機能

  def follow #/users/follow/:id
    user = User.find(params[:id])
    current_user.follow(user) #フォローするメソッド
    redirect_to user_url(user)
  end

  def unfollow #/users/unfollow/:id
    user = User.find(params[:id])
    current_user.stop_following(user)
    redirect_to user_url(user)
  end

  ### フォローリストの描写

  def follow_list #( /users/:id/follow_list )
    @user = User.find(params[:id])
    @users = @user.all_following
  end

  # app/views/users/follow_list.html.erb

  def follower_list #( /users/:id/follower_list )
    @user = User.find(params[:id])
    @users = @user.followers
  end

  # app/views/users/follower_list.html.erb

  private

    def user_params #ストロングパラメータ 許可するパラメータを指定する。
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end

    def correct_user?
      redirect_to root_url unless current_user == User.find(params[:id])
    end
end







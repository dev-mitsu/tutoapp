## 静的ページを扱うコントローラー

class PagesController < ApplicationController
  include SessionsHelper
  before_action :logged_in?, only: :top

  def top
    @tweets = Tweet.all.order(created_at: :desc).page(params[:page]).per(5)
  end

  def about
  end

  private

    def logged_in?
      redirect_to login_url if current_user.nil?
    end
end


#views/pages/about.html.erb
class SessionsController < ApplicationController
  include SessionsHelper #login, logout等実装

  def new #ログインページの描写
  end

  # フォーム入力値はparams[:session][:email], params[:session][:password]で取得できる！

  def create #ログイン処理
    user = User.find_by(email: params[:session][:email])

    if user && user.authenticate(params[:session][:password])
      # session[:user_id] = user.id
      log_in(user)
      redirect_to root_url

    else
      flash[:alert] = "メールアドレスもしくはパスワードが違います。"
      render 'new'
    end
  end

  # session[:user_id] = user.id

  def destroy #ログアウト処理
    logout
    redirect_to root_url
  end
end















class LikesController < ApplicationController
  include SessionsHelper

  def create #いいねボタン
    @like = current_user.likes.create(tweet_id: params[:tweet_id])
    # redirect_to root_url

    @tweet = Tweet.find(params[:tweet_id])

    respond_to do |format| #指定された形式でレスポンスを返す
      format.html { redirect_to root_url } # いつも通りのhtml
      format.js # ajax通信用
    end
  end

  # views/likes/create.js.erb


  # current_user.likes.create()
  # like => user_id:current_user.id

  def destroy #いいね解除ボタン
    like = current_user.likes.find_by(tweet_id: params[:tweet_id])
    like.destroy
    # redirect_to root_url

    @tweet = Tweet.find(params[:tweet_id])

    respond_to do |format|
      format.html { redirect_to root_url }
      format.js
    end
  end
end

# destroy.js.erb

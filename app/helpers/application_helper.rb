module ApplicationHelper

  #◯年◯月◯日
  
  def date_format(datetime)
    y = datetime.year
    m = datetime.month
    d = datetime.day
    
    return "#{y}年#{m}月#{d}日"
  end

  #created_at(datetime)を受け取り、
  #日本語の日付のフォーマット(文字列)に変換して返す

end


class User < ApplicationRecord
  has_secure_password
  validates :email, length: {maximum: 255}
  validates :password, presence: true, length: { minimum: 6 }
  has_many :tweets, dependent: :destroy
  has_many :likes, dependent: :destroy

  acts_as_follower
  acts_as_followable

  def liked_to(tweet)
    self.likes.find_by(tweet_id: tweet.id)
  end

  def following_tweets
    tweets = []
    self.all_following.each do |following_user| #配列！
      tweets.concat(following_user.tweets.last(10))
    end

    # Relation型に変換！
    Tweet.where(id: tweets.map{|tweet| tweet.id})
        .order(created_at: :desc)
  end
end

class Tweet < ApplicationRecord
  belongs_to :user
  has_many :likes, dependent: :destroy

  def like_user(user)
    likes.find_by(user_id: user.id)
  end
end

class CreateTweets < ActiveRecord::Migration[5.2]
  def change
    create_table :tweets do |t|
      t.text :content
      t.references :user, foreign_key: true #外部キー制約
      t.timestamps
    end
  end
end



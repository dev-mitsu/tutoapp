# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# db/seeds.rb


User.create(name: "たけみつ",
            email: "developer.mitsu@gmail.com",
            password: "123456")

# rails db:migrate:reset データベースのリセット
# rails db:seed シードデータの投入
10.times do |i|
  user = User.create(name: "test#{i+1}",
                     email: "test#{i+1}@test.com",
                     password: "testtest")
  5.times do
    user.tweets.create(content: "test"*10)
  end
end

1000.times do
  Like.create(user_id: User.all.sample.id,
              tweet_id: Tweet.all.sample.id)
end

# $ rails db:seed








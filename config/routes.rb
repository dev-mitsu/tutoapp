Rails.application.routes.draw do
  root 'pages#top'
  get    '/login', to: 'sessions#new' #ログインページの表示
  post   '/login', to: 'sessions#create' #ログイン処理
  delete '/logout', to: 'sessions#destroy' #ログアウト処理

  get    '/about', to: 'pages#about'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users # userに関するCRUDに必要なルーティングをすべて用意


  resources :tweets, only: [:create, :destroy, :new, :index]

  resources :tweets do
   resources :likes, only: [:create, :destroy]
  end

  # ここからフォロー
  post 'users/follow/:id',
       to: 'users#follow', as: 'follow'
  delete 'users/unfollow/:id',
         to: 'users#unfollow', as: 'unfollow'


  get 'users/:id/follow_list',
      to: 'users#follow_list',
      as: 'follow_list'

  get 'users/:id/follower_list',
      to: 'users#follower_list',
      as: 'follower_list'
end

# config/routes.rb
# rails info








# tweets/newページにアクセス。
# フォームに投稿内容を記述
# 投稿(submit)ボタンを押す→createアクションの実行
